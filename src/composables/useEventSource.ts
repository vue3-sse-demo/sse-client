import { provide as vueProvide, InjectionKey, onUnmounted } from 'vue';
import { useEventListener } from '@vueuse/core';

export type AppEventSourceListener = (data: object) => void;

export type AppEventSourceAddEventListener = (
  type: string,
  listener: AppEventSourceListener
) => void;

export interface AppEventSource {
  addEventListener: AppEventSourceAddEventListener;
}

export const userEventInjectKey: InjectionKey<AppEventSource> =
  Symbol('userEvent');

export function useEventSource(url: string) {
  // 建立EventSource(SSE instance)
  const es = new EventSource(url);

  // 讓EventSource加入監聽事件的處理函式
  const addEventListener: AppEventSourceAddEventListener = (type, listener) => {
    // 三個參數分別為 1.要加入事件的物件實體 2.事件名稱 3.callback
    // useEventListener還會自動在Component unmounted時把事件註銷掉
    useEventListener<string, MessageEvent>(es, type, (event) =>
      listener(JSON.parse(event.data))
    );
  };

  // 只將特定的屬性或方法暴露出去給子層使用
  // 子層使用vue inject配合對應的InjectKey即可拿到context
  const provide = (key: InjectionKey<AppEventSource>) => {
    const context: AppEventSource = {
      addEventListener,
      // ...other
    };
    vueProvide(key, context);
  };

  // 當Component Unmounted時也一併將它創建的Sse實例關閉連線
  onUnmounted(() => {
    es.close();
  });

  return {
    addEventListener,
    provide,
  };
}
