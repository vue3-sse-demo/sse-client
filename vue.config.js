const { defineConfig } = require('@vue/cli-service');

module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    proxy: {
      '/sse': {
        target: process.env.VUE_APP_SSE_SERVER_URL,
        changeOrigin: true,
        pathRewrite: { '^/sse': '' },
      },
    },
    compress: false,
  },
});
